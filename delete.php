<?php

require_once ('db_connect.php');

$id = (int) $_GET['id'];

if (isset($_GET['confirm'])) {
    $sql = 'delete  from members where id=:id';
    try {
        $query = $pdo->prepare($sql);
        $query->bindValue('id',$id);
        $query->execute();
        header('Location: index.php');
    } catch (Exception $error) {
        echo 'Database Connection Failed';
        die();
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Delete Hotel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/main.css">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-default ">
        <a class="navbar-brand" href="index.php">Common list</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="createStudent.php">Add student data<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="createCoach.php">Add coach data</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="createAdmin.php">Add admin data</a>
                </li>
            </ul>
        </div>
    </nav>
    <h1>Do not change your mind?</h1>
        <a href="index.php" class="btn btn-primary">Cancel</a>
        <a href="delete.php?id=<?=$id?>&confirm=1" class="btn btn-danger">Didn't change my mind</a>
</div>
</body>
</html>