<?php
$persons = [
    [
        'fullName' => 'Zloy D.C.',
        'phone' => '111-11-11',
        'email' => 'zlo@mail.com',
        'role' => 'admin',
        'workingDay' => 'monday'
],
    [
        'fullName' => 'Ymnik F.C.',
        'phone' => '222-22-22',
        'email' => 'ym26@gmail.com',
        'role' => 'student',
        'averageMark' => 9.5
    ],
    [
        'fullName' => 'Petin T',
        'phone' => '333-33-33',
        'email' => 'timur@gmail.comm',
        'role' => 'coach',
        'subject' => 'PHP'
    ],
    [
        'fullName' => 'Drygoy V.I.',
        'phone' => '444-44-44',
        'email' => 'dryg@mail.com',
        'role' => 'coach',
        'subject' => 'English'
    ],
    [
        'fullName' => 'Good O.K.',
        'phone' => '555-55-55',
        'email' => 'good@mail.com',
        'role' => 'admin',
        'workingDay' => 'friday'
    ],[
        'fullName' => 'Balbes I.B.',
        'phone' => '666-66-66',
        'email' => 'zero@gmail.com',
        'role' => 'student',
        'averageMark' => 5.5
    ],
];