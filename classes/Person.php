<?php


class Person
{
 protected $fullName;
 protected $phone;
 protected $email;
 protected $role;

 public function __construct($fullName,$phone,$email,$role)
 {
     $this->fullName = $fullName;
     $this->phone = $phone;
     $this->email = $email;
     $this->role = $role;
 }


    public function getInfo()
 {
     return $this->fullName . ' phone: ' . $this->phone . ' email: ' . $this->email . ' role: ' . $this->role;
 }

}