<?php


class Admin extends Person
{
    private $workingDay;

    public function __construct($fullName, $phone, $email, $role)
    {
        parent::__construct($fullName, $phone, $email, $role);
    }
    public function setWorkingDay($workingDay)
    {
        $this->workingDay = $workingDay;
    }

    public  function getWorkingDay()
    {
    return $this->workingDay;
    }
    public function getInfo()
    {
    return parent::getInfo() . 'workingDay'  . $this->getWorkingDay();
    }
}