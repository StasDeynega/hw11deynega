<?php


class Student extends Person
{
    private $averageMark;

    public function __construct($fullName,$phone,$email,$role)
    {
        parent::__construct($fullName,$phone,$email,$role);
    }

    public function setAverageMark($averageMark){
        $this->averageMark = $averageMark;
    }
    public  function getAverageMark()
    {
        return $this->averageMark;
    }
    public function getInfo()
    {
        return parent::getInfo() . 'averageMark'  . $this->getAverageMark();
    }
}
