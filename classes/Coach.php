<?php


class Coach extends Person
{
    private $subject;

    public function __construct($fullName, $phone, $email, $role)
    {
        parent::__construct($fullName, $phone, $email, $role);
    }
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }
    public  function getSubject()
    {
        return $this->subject;
    }
    public function getInfo()
    {
        return parent::getInfo() . 'subject'  . $this->getSubject();
    }
}


