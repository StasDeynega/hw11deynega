<?php
require_once 'classes/Person.php';
require_once 'classes/Student.php';
require_once 'classes/Admin.php';
require_once 'classes/Coach.php';
require_once 'db_connect.php';

try {
    $sql = 'select * from members';

    $membersQuery = $pdo->query ( $sql );

    $members = $membersQuery ->fetchAll();

} catch (Exception $error) {
    echo 'Database Connection Failed';
    die();
}


//$ass= new Student('sdsd','assas','asas','sdsds ');
//$ass->setAverageMark(15);
//echo $ass->getInfo();

?>

<!DOCTYPE html>
<html >
<head>
    <title>Common list</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/main.css">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-default ">
        <a class="navbar-brand" href="index.php">Common list</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="createStudent.php">Add student data<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="createCoach.php">Add coach data</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="createAdmin.php">Add admin data</a>
                </li>
            </ul>
        </div>
    </nav>

    <h1><center>No one is forgotten</center></h1>

    <table class="table">
        <thead class="thead-dark">
            <th>№</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Role</th>
            <th>Average Mark</th>
            <th>Subject</th>
            <th>Working Day</th>
            <th>Update</th>
            <th>Delete</th>
            </thead>
        <tbody>
        <?php $k = 1; foreach ($members as $person) : ?>
            <tr>
                <td><?=$k++?></td>
                <td><?=$person['fullName']?></td>
                <td><?=$person['email']?></td>
                <td><?=$person['phone']?></td>
                <td><?=$person['role']?></td>
                <td><?=$person['averageMark']?></td>
                <td><?=$person['subject']?></td>
                <td><?=$person['workingDay']?></td>
                <td><a href="edit.php?id=<?=$person['id']?>" class="btn btn-primary">Update</a></td>
                <td><a href="delete.php?id=<?=$person['id']?>" class="btn btn-danger">Delete</a></td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>
</body>
</html>

