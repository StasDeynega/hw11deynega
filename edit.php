<?php

require_once('db_connect.php');

if (!empty($_POST)) {
    try {
        $sql = 'update members set
            fullName=:fullName,
            email=:email,
            phone=:phone,
            role=:role,
            averageMark=:averageMark,
            subject=:subject,
            workingDay=:workingDay
            where id=:id';
        $query = $pdo->prepare($sql);
        $query->bindValue('fullName', $_POST['fullName']);
        $query->bindValue('email', $_POST['email']);
        $query->bindValue('phone', $_POST['phone']);
        $query->bindValue('role', $_POST['role']);
        $query->bindValue('averageMark', $_POST['averageMark']);
        $query->bindValue('subject', $_POST['subject']);
        $query->bindValue('workingDay', $_POST['workingDay']);
        $query->bindValue('id', $_POST['id']);
        if ($query->execute()) {
            $query->closeCursor();
            header('Location: index.php');
        }
    } catch (Exception $exception) {
        echo $exception->getMessage();
        die();
    }
}
try {
    $sql = 'select * from members where id=:id';
    $query = $pdo->prepare($sql);
    $query->bindValue('id', $_GET['id']);
    $query->execute();
    $member = $query->fetch();
} catch (Exception $error) {
    echo 'Database Connection Failed';
    die();
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Edit database</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/main.css">
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-default ">
            <a class="navbar-brand" href="index.php">Common list</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="createStudent.php">Add student data<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="createCoach.php">Add coach data</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="createAdmin.php">Add admin data</a>
                    </li>
                </ul>
            </div>
        </nav>
        <form method="POST">
            <input type="hidden" name="id" value="<?=$member['id']?>">
            <div class="form-group">
                <label for="fullName">Full Name</label>
                    <input type="text" class="form-control" name="fullName" value="<?=$member['fullName']?>">
                <label for="phone">Phone</label>
                    <input type="text" class="form-control" name="phone" value="<?=$member['phone']?>">
                <label for="email">Email</label>
                    <input type="text" class="form-control" name="email" value="<?=$member['email']?>">
                <label for="role">Role</label>
                    <input type="text" class="form-control" name="role" value="<?=$member['role']?>">
                <label for="subject">Average Mark</label>
                    <input type="text" class="form-control" name="averageMark" value="<?=$member['averageMark']?>">
                <label for="subject">Subject</label>
                    <input type="text" class="form-control" name="subject" value="<?=$member['subject']?>">
                <label for="workingDay">Working Day</label>
                    <input type="text" class="form-control" name="workingDay" value="<?=$member['workingDay']?>">

                <button class="btn btn-success">Update</button>
            </div>
        </form>
    </div>
</body>
</html>
