<?php

try {
    $pdo = new PDO(
        'mysql:host=localhost;dbname=lms',
        'lms_user',
        '123456'
    );
    
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $error) {
 	echo 'Database Connection Failed';
	die();
}
