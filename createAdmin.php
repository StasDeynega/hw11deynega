<?php

require_once('db_connect.php');


if (isset($_POST['role'])) {
    try {
        $sql = 'insert into members set 
            fullName=:fullName,
            email=:email,
            phone=:phone,
            role=:role,
            workingDay=:workingDay
           ';
        $query = $pdo->prepare($sql);
        $query->bindValue('fullName', $_POST['fullName']);
        $query->bindValue('email', $_POST['email']);
        $query->bindValue('phone', $_POST['phone']);
        $query->bindValue('role', $_POST['role']);
        $query->bindValue('workingDay', $_POST['workingDay']);
        $query->execute();
            header('Location: index.php');
    }  catch (Exception $error) {
        echo 'Database Connection Failed';
        die();
    }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Create new member</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/main.css">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-default ">
        <a class="navbar-brand" href="index.php">Common list</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="createStudent.php">Add student data<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="createCoach.php">Add coach data</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="createAdmin.php">Add admin data</a>
                </li>
            </ul>
        </div>
    </nav>

    <h1>Create new admin</h1>
    <form method="POST">
        <input type="hidden" name="id" value="<?=$member['id']?>">
        <div>
            <label for="fullName">Full Name</label>
            <input type="text" class="form-control" name="fullName">
        </div>
        <div>
            <label for="phone">Phone</label>
            <input type="text" class="form-control" name="phone">
        </div>
        <div>
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email">
        </div>
        <div>
            <label for="role">Role</label>
            <input type="text" class="form-control" name="role" value="admin">
        </div>
        <div>
            <label for="workingDay">Working Day</label>
            <input type="text" class="form-control" name="workingDay">
        </div>
        <div>
            <button class="btn btn-success">Add new admin</button>
        </div>
    </form>
</div>
</body>
</html>