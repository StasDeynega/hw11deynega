<?php
require_once 'dataPerson.php';
try {
$pdo = new PDO(
    'mysql:host=localhost;dbname=lms',
    'lms_user',
    '123456'
);

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = '
    create table if not exists members (
        id int not null auto_increment primary key ,
        fullName varchar (255),
        phone varchar (255),
        email varchar (255),
        role varchar (255),
        averageMark float, 
        subject varchar (255),
        workingDay varchar (255)
    )
    ';

$pdo->exec($sql);

foreach ($persons as $person) {
    if ($person['role'] == 'student') {
        $sql = '
    INSERT INTO members SET 
        fullName=:fullName,
        phone=:phone,
        email=:email,
        role=:role,
        averageMark=:averageMark
';
        $query = $pdo->prepare($sql);

        $query->bindValue(':fullName', $person['fullName']);
        $query->bindValue(':phone', $person['phone']);
        $query->bindValue(':email', $person['email']);
        $query->bindValue(':role', $person['role']);
        $query->bindValue(':averageMark', $person['averageMark']);

        $query->execute();
    }elseif($person['role'] == 'admin') {
        $sql = '
    INSERT INTO members SET 
        fullName=:fullName,
        phone=:phone,
        email=:email,
        role=:role,
        workingDay=:workingDay
';
        $query = $pdo->prepare($sql);

        $query->bindValue(':fullName', $person['fullName']);
        $query->bindValue(':phone', $person['phone']);
        $query->bindValue(':email', $person['email']);
        $query->bindValue(':role', $person['role']);
        $query->bindValue(':workingDay', $person['workingDay']);

        $query->execute();
    }elseif($person['role'] == 'coach') {
        $sql = '
    INSERT INTO members SET 
        fullName=:fullName,
        phone=:phone,
        email=:email,
        role=:role,
        subject=:subject
';
        $query = $pdo->prepare($sql);

        $query->bindValue(':fullName', $person['fullName']);
        $query->bindValue(':phone', $person['phone']);
        $query->bindValue(':email', $person['email']);
        $query->bindValue(':role', $person['role']);
        $query->bindValue(':subject', $person['subject']);

        $query->execute();
    }
}
} catch (Exception $error) {
    echo 'Database Connection Failed';
    die();
}

echo 'migrate complete';